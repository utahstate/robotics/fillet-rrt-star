# Rapidly searching Random Trees

This repository implements multiple RRT searching algorithms and an implementation of a Kalman Filter with a generic C++ interface.
Most of the repository have been implanted fairly thoroughly but some part are actively being updated and implemented.

## Installation

Other then ROS 2 and Eigen 3 this repository has only one other dependency, Flann.
Flann is a library that implements fast multi-dimensional searching algorithms.
To install Flann run the command below.

    sudo apt-get install -y libflann-dev

There are a couple ROS repository needed that isn't installed by default.
To install it run the following command substituting "melodic" for the ROS distribution you are using.

    sudo apt-get install ros-melodic-rviz-visual-tools

## Algorithms Provided

The table below lists each of the algorithms that this package implements, the variations each algorithm supports, and the names of the papers that purposed them.

| Algorithm                       | K Nearest Searches | Fillet-Based | Source      |
|---------------------------------|--------------------|--------------|-------------|
| RRT                             | Yes                | Yes          | [[1](#1)]   |
| Bidirectional RRT               | No                 | No           | [[2](#2)]   |
| RRT Connect                     | No                 | No           | [[3](#3)]   |
| RRT\*                           | Yes                | Yes          | [[4](#4)]   |
| Intelligent Bidirectional RRT\* | No                 | No           | [[5](#5)]   |
| RRT\* Smart                     | Yes                | Yes          | [[6](#6)]   |
| Smart and Informed RRT\*        | Yes                | Yes          | Novel       |
| BIT\*                           | No                 | Yes          | [[10](#10)] |
| Source of the variants          | [[7](#7)]          | Novel        |             |

## Sections

### rrt_search
This is where most of the path planning functionality is implemented and where the C++ API for the path planning may be found.
This includes various path planning algorithms as well as various edge generators that can be used with them.

### kalman_filter
This is where the Kalman Filter equations are implemented.
This includes the state propagation and measurement update equations, functions for running Monte Carlo and LinCov simulations, and functions for plotting the meaningful statistics after running a Mote Carlo or LinCov simulation.
This package also contains various sensor and dynamic models that can be used with the Kalman Filter code.

### occupancy_grid
This package implements a basic occupancy grid data structure using OpenCV.

### matplotlibcpp
This is a fork of an open source C++ wrapper around python's matplotlib module.
It is used here for plotting simulation results.
This fork is modified to compile and link correctly in a ROS 2 workspace.
The original project can be found [here](https://github.com/lava/matplotlib-cpp).

### visibility_graph
This package implements the well know visibility graph path planner and is used in the rrt_search package.

### radar_detection
This package provides the implementation of heuristics for the probability of detection by signal pulse radar station.
It is used by the path planners when avoiding detection is mission critical.

## Known Issues

1. Things that needs to be implemented or reimplemented
  * Bidirectional-RRT variants
  * Fillet-Based bidirectional variants

## References
<a id="1">[1]</a>
S. LaValle (1998) Rapidly-exploring random trees: A new tool for path planning

<a id="2">[2]</a>
M. Jordan, A. Perex (2013) Optimal bidirectional rapidly-exploring random trees

<a id="3">[3]</a>
J. Kuffner, S. LaValle (2000) RRT-Connect: An Efficient Approach to Single-Query Path Planning

<a id="4">[4]</a>
S. Karaman, E. Frazzoli (2011) Sampling-based algorithms for optimal motion planning

<a id="5">[5]</a>
A. Qureshi, Y. Ayaz (2015) Intelligent bidirectional rapidly-exploring random trees for optimal motion planning in complex cluttered environments

<a id="6">[6]</a>
J. Nasir, F. Islam, U. Malik, Y. Ayaz, O. Hasan, M. Khan, M. Muhammad (2013) RRT\*-SMART: A Rapid Convergence Implementation of RRT\*

<a id="7">[7]</a>
S. LaValle (2006) Planning Algorithms

<a id="8">[8]</a>
C. Moon, W. Chung (2015) Kinodynamic Planning Dual-Tree RRT (DT-RRT) for Two-Wheeled Mobil robots Using the Rapidly Exploring Random Tree

<a id="9">[9]</a>
Moll M, Sucan IA and Kavraki LE (2015) Benchmarking motion planning algorithms: An extensible infrastructure for analysis and visualization. IEEE Robotics Automation Magazine 22(3): 96–102.

<a id="10">[10]</a>
Gammell JD, Barfoot TD, Srinivasa SS. Batch Informed Trees (BIT*): Informed asymptotically optimal anytime search. The International Journal of Robotics Research. 2020;39(5):543-567. doi:10.1177/0278364919890396

