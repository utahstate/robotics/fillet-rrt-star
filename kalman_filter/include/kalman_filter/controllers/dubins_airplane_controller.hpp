/**
 * @File: dubins_airplane_controller.hpp
 * @Date: July 2023
 * @Author: James Swedeen
 *
 * @brief
 * A controller class for the Dubin's Airplane.
 **/

#ifndef KALMAN_FILTER_CONTROLLERS_DUBINS_AIRPLANE_CONTROLLER_HPP
#define KALMAN_FILTER_CONTROLLERS_DUBINS_AIRPLANE_CONTROLLER_HPP

/* C++ Headers */
#include<cstdint>
#include<memory>
#include<limits>

/* Eigen Headers */
#include<Eigen/Dense>

/* Local Headers */
#include<kalman_filter/controllers/controller_base.hpp>
#include<kalman_filter/math/quaternion.hpp>
//#include<kalman_filter/math/inf_time_lqr.hpp>
//#include<kalman_filter/dynamics/dubins_airplane_model.hpp>
//#include<kalman_filter/mappings/dubins_airplane_mapping.hpp>

namespace kf
{
namespace control
{
template<typename DIM_S, typename SCALAR, Eigen::StorageOptions OPTIONS>
class DubinsAirplaneController;

template<typename DIM_S, typename SCALAR = double, Eigen::StorageOptions OPTIONS = Eigen::RowMajor>
using DubinsAirplaneControllerPtr = std::shared_ptr<DubinsAirplaneController<DIM_S,SCALAR,OPTIONS>>;

/**
 * @DIM_S
 * The type of a Dimensions object or an inheriting object that has information about the size of the state vectors.
 *
 * @SCALAR
 * The object type that each dimension will be represented with.
 *
 * @OPTIONS
 * Eigen Matrix options.
 **/
template<typename DIM_S, typename SCALAR = double, Eigen::StorageOptions OPTIONS = Eigen::RowMajor>
class DubinsAirplaneController
 : public ControllerBase<DIM_S,SCALAR,OPTIONS>
{
public:
  /**
   * @Default Constructor
   **/
  DubinsAirplaneController() = delete;
  /**
   * @Copy Constructor
   **/
  DubinsAirplaneController(const DubinsAirplaneController&) noexcept = default;
  /**
   * @Move Constructor
   **/
  DubinsAirplaneController(DubinsAirplaneController&&) noexcept = default;
  /**
   * @Constructor
   *
   * @brief
   * Initializes the object for use.
   *
   * @parameters
   * nominal_vel: The nominal velocity of the aircraft
   * yaw_inf_error: Max yaw error along the reference
   * lat_error_gain: Gain that controls how fast lateral errors are corrected
   * forward_proportional_gain:
   * forward_integral_gain:
   * vel_regularization_gain:
   * yaw_proportional_gain:
   * yaw_integral_gain:
   * roll_regularization_gain:
   * down_proportional_gain:
   * down_integral_gain:
   * pitch_regularization_gain:
   * control_upper_bound: The upper bound on the allowed control
   * control_lower_bound: The lower bound on the allowed control
   **/
  DubinsAirplaneController(const SCALAR                                                                nominal_vel,
                           const SCALAR                                                                yaw_inf_error,
                           const SCALAR                                                                lat_error_gain,
                           const SCALAR                                                                forward_proportional_gain,
                           const SCALAR                                                                forward_integral_gain,
                           const SCALAR                                                                vel_regularization_gain,
                           const SCALAR                                                                yaw_proportional_gain,
                           const SCALAR                                                                yaw_integral_gain,
                           const SCALAR                                                                roll_regularization_gain,
                           const SCALAR                                                                down_proportional_gain,
                           const SCALAR                                                                down_integral_gain,
                           const SCALAR                                                                pitch_regularization_gain,
                           const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::CONTROL_DIM,OPTIONS>>& control_upper_bound,
                           const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::CONTROL_DIM,OPTIONS>>& control_lower_bound);
  /**
   * @Deconstructor
   **/
  ~DubinsAirplaneController() noexcept override = default;
  /**
   * @Assignment Operators
   **/
  DubinsAirplaneController& operator=(const DubinsAirplaneController&)  noexcept = default;
  DubinsAirplaneController& operator=(      DubinsAirplaneController&&) noexcept = default;
  /**
   * @getControl
   *
   * @brief
   * Used to find the control input for this step in the simulation.
   *
   * @parameters
   * time: The current simulation time
   * nav_state: The current navigation state vector
   * ref_state: The current reference state vector
   * next_ref_state: The next reference state vector
   * sim_dt: The time delta between the current and next reference states
   *
   * @return
   * The control for this time step.
   **/
  inline Eigen::Matrix<SCALAR,1,DIM_S::CONTROL_DIM,OPTIONS>
    getControl(const SCALAR                                                            time,
               const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::NAV_DIM,OPTIONS>>& nav_state,
               const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::REF_DIM,OPTIONS>>& ref_state,
               const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::REF_DIM,OPTIONS>>& next_ref_state,
               const SCALAR                                                            sim_dt) override;
  /**
   * @getControlPDWRErrorState
   *
   * @brief
   * Used to find the partial derivative of the control with respect to the error state.
   *
   * @parameters
   * time: The current simulation time
   * nav_state: The current navigation state vector
   * ref_state: The current reference state vector
   *
   * @return
   * The control for this time step.
   **/
  inline Eigen::Matrix<SCALAR,DIM_S::CONTROL_DIM,DIM_S::ERROR_DIM,OPTIONS>
    getControlPDWRErrorState(const SCALAR                                                            time,
                             const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::NAV_DIM,OPTIONS>>& nav_state,
                             const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::REF_DIM,OPTIONS>>& ref_state) override;
private:
  SCALAR                                             nominal_vel;
  SCALAR                                             yaw_inf_error;
  SCALAR                                             lat_error_gain;
  SCALAR                                             forward_proportional_gain;
  SCALAR                                             forward_integral_gain;
  SCALAR                                             vel_regularization_gain;
  SCALAR                                             yaw_proportional_gain;
  SCALAR                                             yaw_integral_gain;
  SCALAR                                             roll_regularization_gain;
  SCALAR                                             down_proportional_gain;
  SCALAR                                             down_integral_gain;
  SCALAR                                             pitch_regularization_gain;
  Eigen::Matrix<SCALAR,1,DIM_S::CONTROL_DIM,OPTIONS> control_upper_bound;
  Eigen::Matrix<SCALAR,1,DIM_S::CONTROL_DIM,OPTIONS> control_lower_bound;
};


template<typename DIM_S, typename SCALAR, Eigen::StorageOptions OPTIONS>
DubinsAirplaneController<DIM_S,SCALAR,OPTIONS>::
  DubinsAirplaneController(const SCALAR                                                                nominal_vel,
                           const SCALAR                                                                yaw_inf_error,
                           const SCALAR                                                                lat_error_gain,
                           const SCALAR                                                                forward_proportional_gain,
                           const SCALAR                                                                forward_integral_gain,
                           const SCALAR                                                                vel_regularization_gain,
                           const SCALAR                                                                yaw_proportional_gain,
                           const SCALAR                                                                yaw_integral_gain,
                           const SCALAR                                                                roll_regularization_gain,
                           const SCALAR                                                                down_proportional_gain,
                           const SCALAR                                                                down_integral_gain,
                           const SCALAR                                                                pitch_regularization_gain,
                           const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::CONTROL_DIM,OPTIONS>>& control_upper_bound,
                           const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::CONTROL_DIM,OPTIONS>>& control_lower_bound)
 : nominal_vel(nominal_vel),
   yaw_inf_error(yaw_inf_error),
   lat_error_gain(lat_error_gain),
   forward_proportional_gain(forward_proportional_gain),
   forward_integral_gain(forward_integral_gain),
   vel_regularization_gain(vel_regularization_gain),
   yaw_proportional_gain(yaw_proportional_gain),
   yaw_integral_gain(yaw_integral_gain),
   roll_regularization_gain(roll_regularization_gain),
   down_proportional_gain(down_proportional_gain),
   down_integral_gain(down_integral_gain),
   pitch_regularization_gain(pitch_regularization_gain),
   control_upper_bound(control_upper_bound),
   control_lower_bound(control_lower_bound)
{}

template<typename DIM_S, typename SCALAR, Eigen::StorageOptions OPTIONS>
inline Eigen::Matrix<SCALAR,1,DIM_S::CONTROL_DIM,OPTIONS> DubinsAirplaneController<DIM_S,SCALAR,OPTIONS>::
  getControl(const SCALAR                                                            time,
             const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::NAV_DIM,OPTIONS>>& nav_state,
             const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::REF_DIM,OPTIONS>>& ref_state,
             const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::REF_DIM,OPTIONS>>& next_ref_state,
             const SCALAR                                                            sim_dt)
{
  const Eigen::Matrix<SCALAR,3,3,OPTIONS> ned_to_path_rotation = math::quat::rollPitchYawToDirectionCosineMatrix<SCALAR,OPTIONS>(0, 0, ref_state[DIM_S::REF::YAW_IND]).transpose();
  const Eigen::Matrix<SCALAR,1,3,OPTIONS> path_error           = ned_to_path_rotation * (ref_state.template middleCols<3>(DIM_S::REF::POS_START_IND) - nav_state.template middleCols<3>(DIM_S::NAV::POS_START_IND)).transpose();
  const Eigen::Matrix<SCALAR,1,3,OPTIONS> nav_rpy              = math::quat::quaternionToRollPitchYaw(nav_state.template middleCols<4>(DIM_S::NAV::QUAT_START_IND));

  const SCALAR commanded_yaw = ref_state[DIM_S::REF::YAW_IND] -
                               (this->yaw_inf_error * (SCALAR(2)/math::pi<SCALAR>()) * std::atan(this->lat_error_gain * path_error[1]));
  const SCALAR yaw_error = nav_rpy[2] - commanded_yaw;

  const SCALAR roll_error  = ref_state[DIM_S::REF::ROLL_IND]  - nav_rpy[0];
  const SCALAR pitch_error = ref_state[DIM_S::REF::PITCH_IND] - nav_rpy[1];

  Eigen::Matrix<SCALAR,1,DIM_S::CONTROL_DIM,OPTIONS> feedback;

  feedback[DIM_S::CONTROL::ROLL_RATE_IND] = (this->yaw_proportional_gain    * std::atan2(std::sin(yaw_error), std::cos(yaw_error))) +
                                            (this->yaw_integral_gain        * nav_state[DIM_S::NAV::YAW_ERROR_INT_IND]) +
                                            (this->roll_regularization_gain * std::atan2(std::sin(roll_error), std::cos(roll_error)));

  feedback[DIM_S::CONTROL::PITCH_RATE_IND] = (this->down_proportional_gain    * -path_error[2]) +
                                             (this->down_integral_gain        * nav_state[DIM_S::NAV::DOWN_ERROR_INT_IND]) +
                                             (this->pitch_regularization_gain * std::atan2(std::sin(pitch_error), std::cos(pitch_error)));

  feedback[DIM_S::CONTROL::AIR_SPEED_RATE_IND] = (this->forward_proportional_gain * path_error[0]) +
                                                 (this->forward_integral_gain     * nav_state[DIM_S::NAV::ALONG_TRACK_POS_ERROR_INT_IND]) +
                                                 (this->vel_regularization_gain   * (this->nominal_vel - nav_state.template middleCols<3>(DIM_S::NAV::VEL_START_IND).norm()));

  // Find feed forward term
/*  Eigen::Matrix<SCALAR,1,DIM_S::TRUTH_DIM,OPTIONS> ff_error_vec =
    next_ref_state_truth.transpose() - ((Eigen::Matrix<SCALAR,DIM_S::TRUTH_DISP_DIM,DIM_S::TRUTH_DISP_DIM,OPTIONS>::Identity() + (sim_dt * truth_dynamics_mat)) * ref_state_truth.transpose());
  ff_error_vec[DIM_S::TRUTH::ROLL_IND]  = std::atan2(std::sin(ff_error_vec[DIM_S::TRUTH::ROLL_IND]),  std::cos(ff_error_vec[DIM_S::TRUTH::ROLL_IND]));
  ff_error_vec[DIM_S::TRUTH::PITCH_IND] = std::atan2(std::sin(ff_error_vec[DIM_S::TRUTH::PITCH_IND]), std::cos(ff_error_vec[DIM_S::TRUTH::PITCH_IND]));
  ff_error_vec[DIM_S::TRUTH::YAW_IND]   = std::atan2(std::sin(ff_error_vec[DIM_S::TRUTH::YAW_IND]),   std::cos(ff_error_vec[DIM_S::TRUTH::YAW_IND]));

  const Eigen::Matrix<SCALAR,1,DIM_S::CONTROL_DIM,OPTIONS> feed_forward =
    truth_control_mat.transpose().colPivHouseholderQr().solve(ff_error_vec.transpose() * (SCALAR(1)/sim_dt));

  return feedback + feed_forward;*/

  return feedback;
}

template<typename DIM_S, typename SCALAR, Eigen::StorageOptions OPTIONS>
inline Eigen::Matrix<SCALAR,DIM_S::CONTROL_DIM,DIM_S::ERROR_DIM,OPTIONS> DubinsAirplaneController<DIM_S,SCALAR,OPTIONS>::
  getControlPDWRErrorState(const SCALAR                                                            time,
                           const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::NAV_DIM,OPTIONS>>& nav_state,
                           const Eigen::Ref<const Eigen::Matrix<SCALAR,1,DIM_S::REF_DIM,OPTIONS>>& ref_state)
{
  const Eigen::Matrix<SCALAR,3,3,OPTIONS> ned_to_path_rotation = math::quat::rollPitchYawToDirectionCosineMatrix<SCALAR,OPTIONS>(0, 0, ref_state[DIM_S::REF::YAW_IND]).transpose();
  const Eigen::Matrix<SCALAR,1,3,OPTIONS> path_error           = ned_to_path_rotation * (ref_state.template middleCols<3>(DIM_S::REF::POS_START_IND) - nav_state.template middleCols<3>(DIM_S::NAV::POS_START_IND)).transpose();
  const Eigen::Matrix<SCALAR,1,3,OPTIONS> nav_rpy              = math::quat::quaternionToRollPitchYaw(nav_state.template middleCols<4>(DIM_S::NAV::QUAT_START_IND));

  Eigen::Matrix<SCALAR,DIM_S::CONTROL_DIM,DIM_S::ERROR_DIM,OPTIONS> output;
  output.setZero();

  output(DIM_S::CONTROL::ROLL_RATE_IND, DIM_S::ERROR::YAW_IND) = this->yaw_proportional_gain;
  output.template block<1,3>(DIM_S::CONTROL::ROLL_RATE_IND, DIM_S::ERROR::POS_START_IND) =
    -(this->yaw_proportional_gain * this->yaw_inf_error * (SCALAR(2)/math::pi<SCALAR>()) * (this->lat_error_gain/std::pow(std::cos(this->lat_error_gain * path_error[1]), SCALAR(2)))) *
    ned_to_path_rotation.row(1);
  output(DIM_S::CONTROL::ROLL_RATE_IND, DIM_S::ERROR::YAW_ERROR_INT_IND) = this->yaw_integral_gain;
  output(DIM_S::CONTROL::ROLL_RATE_IND, DIM_S::ERROR::ROLL_IND) = -this->roll_regularization_gain;

  output(DIM_S::CONTROL::PITCH_RATE_IND, DIM_S::ERROR::DOWN_IND) = this->down_proportional_gain;
  output(DIM_S::CONTROL::PITCH_RATE_IND, DIM_S::ERROR::DOWN_ERROR_INT_IND) = this->down_integral_gain;
  output(DIM_S::CONTROL::PITCH_RATE_IND, DIM_S::ERROR::PITCH_IND) = -this->pitch_regularization_gain;

  output.template block<1,3>(DIM_S::CONTROL::AIR_SPEED_RATE_IND, DIM_S::ERROR::POS_START_IND) = -this->forward_proportional_gain * ned_to_path_rotation.row(0);
  output(DIM_S::CONTROL::AIR_SPEED_RATE_IND, DIM_S::ERROR::ALONG_TRACK_POS_ERROR_INT_IND) = this->forward_integral_gain;
  output.template block<1,3>(DIM_S::CONTROL::AIR_SPEED_RATE_IND, DIM_S::ERROR::VEL_START_IND) =
    -this->vel_regularization_gain * nav_state.template middleCols<3>(DIM_S::NAV::VEL_START_IND).normalized();

  return output;
}
} // namespace control
} // namespace kf

#endif
/* dubins_airplane_controller.hpp */
