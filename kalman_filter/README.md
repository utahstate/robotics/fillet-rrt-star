# Kalman Filter

This package is where the Kalman Filter equations are implemented.
This includes the state propagation equations, measurement update equations, functions for running Monte Carlo and LinCov simulations, functions for generating error budgets, and functions for plotting the meaningful statistics after running a Mote Carlo or LinCov simulation.
This package also contains various sensor and dynamic models that can be used with the Kalman Filter code.

Code Architecture Diagram
<img src="./code_architecture_diagram.svg">

## Rocket Sled Demonstration
This package includes a simple demonstration of how to use the code provided to perform Monte Carlo and LinCov simulations, then plot the results.
The demonstration code can be found at [rocket_sled_demo.cpp](src/rocket_sled_demo.cpp).

The dynamics used in the demonstration are of a one-dimensional rocket sled with measurements of position and velocity.
The dynamics used were borrowed from [[1](#1)] with the following changes

  * The truth and navigation models include position as the integral of velocity.
  * A measurement on the position of the UAV has been added so the position variance remains bounded.
  * The differential drag term on the dynamics of the velocity term has been left out for simplicity.
  * The controller used here is simpler then that described in [[1](#1)] as we use a state feedback control law with a desired position and velocity to reach.

Once the code has been built and the workspace sourced, the rocket sled example can be ran with the following command.
```bash
    ros2 run kalman_filter rocket_sled_demo
```
The program will not end until all of the generated plots are closed.

## Error Budget Demonstration
The rrt_search package contains a node that can be used to generate error budges of the estimation errors a fixed wing UAV experiences while passing through GPS denied areas.
The node reads in waypoints for the UAV dynamics to follow and GPS denied area information from the [ol_error_budget_demo.launch.py](../rrt_search/launch/ol_error_budget_demo.launch.py) ROS launch file.
After simulating the UAV along the provided waypoints the node generates an error budget analysis and plots the results.

Once the code has been built and the workspace sourced, the error budget example can be ran with the following command.
```bash
    ros2 launch rrt_search ol_error_budget_demo.launch.py
```
The program will not end until all of the generated plots are closed.

To change the trajectory the UAV takes or the location of the GPS denied areas the `ol_error_budget_demo.launch.py` launch file located in the rrt_search package can be modified.
The `waypoints_dict` variable controls the number of waypoints that the UAV follows and each waypoints location.
The `gps_denied_dict` variable controls the number and location of the GPS denied areas.

## References
<a id="1">[1]</a>
Christensen RS, Geller D. Linear covariance techniques for closed-loop guidance navigation and control system design and analysis. Proceedings of the Institution of Mechanical Engineers, Part G: Journal of Aerospace Engineering. 2014;228(1):44-65. doi:10.1177/0954410012467717

