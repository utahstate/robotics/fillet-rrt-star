#! /bin/bash

# Exit when any command fails
set -e

home_dir='/home/james/ros_ws/benchmark/'
log_dir="${home_dir}output/"
ompl_stats_script="${home_dir}ompl_benchmark_statistics.py"
plannerarena_dir="${home_dir}plannerarena/"

rm -f "${plannerarena_dir}www/benchmark.db"

#for dir in $log_dir/*/; do
#  for log_file in $dir*.log; do
#    python3 $ompl_stats_script --database "${plannerarena_dir}www/benchmark.db" --append $log_file
#  done
#done

python3 $ompl_stats_script --database "${plannerarena_dir}www/benchmark.db" --append $log_dir*.log

#for log_file in $log_dir*.log; do
#  python3 $ompl_stats_script --database "${plannerarena_dir}www/benchmark.db" --append $log_file
#done

sudo Rscript "${plannerarena_dir}plannerarena"

