/**
 * @File: generate_csv_file.cpp
 * @Date: March 2022
 * @Author: James Swedeen
 *
 * @brief
 * Used to generate CSV files that are then compared to Matlab results.
 **/

/* C++ Headers */
#include<iostream>
#include<fstream>
#include<string>
#include<iomanip>
#include<list>

/* Local Headers */
#include<radar_detection/radar_detection.hpp>

void toCSV(const rd::CrossSectionModelPtrd&                            cross_section_model,
           const std::list<Eigen::Matrix<double,1,6,Eigen::RowMajor>>& aircraft_pose,
           const std::list<Eigen::Matrix<double,1,3,Eigen::RowMajor>>& radar_position,
           const std::string&                                          file_name);

int main(int, char**)
{
  rd::CrossSectionModelPtrd                            cross_section_model;
  std::list<Eigen::Matrix<double,1,6,Eigen::RowMajor>> aircraft_pose;
  std::list<Eigen::Matrix<double,1,3,Eigen::RowMajor>> radar_position;

  for(double north_it = -1000; north_it < 1000; north_it += 250)
  {
  for(double east_it = -1000; east_it < 1000; east_it += 250)
  {
  for(double down_it = -1000; down_it < 0; down_it += 250)
  {
  for(double roll_it = 0; roll_it < rd::twoPi<double>(); roll_it += 1.5)
  {
  for(double pitch_it = 0; pitch_it < rd::twoPi<double>(); pitch_it += 1.5)
  {
  for(double yaw_it = 0; yaw_it < rd::twoPi<double>(); yaw_it += 1.5)
  {
    aircraft_pose.emplace_back((Eigen::Matrix<double,1,6,Eigen::RowMajor>() << north_it, east_it, down_it, roll_it, pitch_it, yaw_it).finished());
  }
  }
  }
  }
  }
  }
  for(double north_it = -10; north_it < 10; north_it += 5.5)
  {
  for(double east_it = -10; east_it < 10; east_it += 5.5)
  {
  for(double down_it = -10; down_it < 0; down_it += 5.5)
  {
    radar_position.emplace_back((Eigen::Matrix<double,1,3,Eigen::RowMajor>() << north_it, east_it, down_it).finished());
  }
  }
  }


  cross_section_model = std::make_shared<rd::ConstantCrossSectionModeld>(0.2);
  toCSV(cross_section_model, aircraft_pose, radar_position, "/home/james/catkin_ws/const_rcs.csv");
  cross_section_model = std::make_shared<rd::EllipsoidCrossSectionModeld>(0.25, 0.15, 0.17);
  toCSV(cross_section_model, aircraft_pose, radar_position, "/home/james/catkin_ws/ellipsoid_rcs.csv");
  cross_section_model = std::make_shared<rd::SpikeballCrossSectionModeld>(4, 0.1, 0.18);
  toCSV(cross_section_model, aircraft_pose, radar_position, "/home/james/catkin_ws/spikeball_rcs.csv");

  exit(EXIT_SUCCESS);
}

void toCSV(const rd::CrossSectionModelPtrd&                            cross_section_model,
           const std::list<Eigen::Matrix<double,1,6,Eigen::RowMajor>>& aircraft_pose,
           const std::list<Eigen::Matrix<double,1,3,Eigen::RowMajor>>& radar_position,
           const std::string&                                          file_name)
{
  std::ofstream file(file_name);
  file << std::setprecision(100);

  file << "Radar North," <<
          "Radar East," <<
          "Radar Down," <<
          "Aircraft North," <<
          "Aircraft East," <<
          "Aircraft Down," <<
          "Aircraft Roll," <<
          "Aircraft Pitch," <<
          "Aircraft Yaw," <<
          "Cross Section\n";

  for(auto radar_pos_it = radar_position.cbegin(); radar_pos_it != radar_position.cend(); ++radar_pos_it)
  {
    for(auto craft_it = aircraft_pose.cbegin(); craft_it != aircraft_pose.cend(); ++craft_it)
    {
      const Eigen::Matrix<double,1,1,Eigen::RowMajor> cross_section =
        cross_section_model->findCrossSection(*craft_it, *radar_pos_it);

        file << radar_pos_it->col(rd::RADAR_IND::NORTH) << ","
             << radar_pos_it->col(rd::RADAR_IND::EAST) << ","
             << radar_pos_it->col(rd::RADAR_IND::DOWN) << ","
             << craft_it->col(rd::AC_IND::NORTH) << ","
             << craft_it->col(rd::AC_IND::EAST) << ","
             << craft_it->col(rd::AC_IND::DOWN) << ","
             << craft_it->col(rd::AC_IND::ROLL) << ","
             << craft_it->col(rd::AC_IND::PITCH) << ","
             << craft_it->col(rd::AC_IND::YAW) << ","
             << cross_section[0] << "\n";
    }
  }

  file.flush();
  file.close();
}

/* generate_csv_file.cpp */
