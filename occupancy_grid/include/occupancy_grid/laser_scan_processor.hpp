/**
 * @File: laser_scan_processor.hpp
 * @Date: June 2020
 * @Author: James Swedeen
 *
 * @brief
 * Used to process incoming laser scan information.
 **/

#ifndef OCCUPANCY_GRID_LASER_SCAN_PROCESSOR_HPP
#define OCCUPANCY_GRID_LASER_SCAN_PROCESSOR_HPP

/* ROS Headers */
#include<rclcpp/rclcpp.hpp>
#include<sensor_msgs/msg/laser_scan.hpp>

/* Eigen Headers */
#include<Eigen/Dense>

/* TF Headers */
#include<tf2_ros/buffer.h>
#include<tf2_ros/transform_listener.h>

/* C++ Headers */
#include<list>
#include<string>
#include<array>

class LaserScanProcessor
 : protected rclcpp::Node
{
public:
  /**
   * @Default Constructor
   **/
  LaserScanProcessor() = delete;
  /**
   * @Copy Constructor
   **/
  LaserScanProcessor(const LaserScanProcessor&) = delete;
  /**
   * @Move Constructor
   **/
  LaserScanProcessor(LaserScanProcessor&&) = delete;
  /**
   * @Constructor
   *
   * @brief
   * Initializes the occupancy grid for use.
   *
   * @parameters
   * node_name: The name of the sub node this class will make
   * laser_scan_topic: The topic that laser scan messages come in on
   * map_tf_id: The TF frame of the global map
   * queue_length: How many laser scan messages should this object hold
   *               between calls of getObstacles
   * node_namespace: The namespace of the node this class will make
   **/
  LaserScanProcessor(const std::string& node_name,
                     const std::string& laser_scan_topic,
                     const std::string& map_tf_id,
                     const size_t       queue_length   = 100,
                     const std::string& node_namespace = std::string());
  /**
   * @Deconstructor
   **/
  ~LaserScanProcessor() noexcept = default;
  /**
   * @Assignment Operators
   **/
  LaserScanProcessor& operator=(const LaserScanProcessor&)  = delete;
  LaserScanProcessor& operator=(      LaserScanProcessor&&) = delete;
  /**
   * @getObstacles
   *
   * @brief
   * Returns the list of known obstacles that this object has collected and clears
   * the internal stash.
   **/
  std::list<Eigen::Matrix<double,2,1>> getObstacles();
private:
  /* Information inflow */
  tf2_ros::Buffer                                              tf_buffer;
  tf2_ros::TransformListener                                   transform_listener;
  std::string                                                  map_tf_id;
  rclcpp::Subscription<sensor_msgs::msg::LaserScan>::SharedPtr laser_scan_sub;
  std::list<Eigen::Matrix<double,2,1>>                         obstacles;
  /**
   * @laserScanCallback
   **/
  void laserScanCallback(const sensor_msgs::msg::LaserScan& msg);
};

#endif
/* laser_scan_processor.hpp */
