/**
 * @File: laser_scan_processor.cpp
 * @Date: June 2020
 * @Author: James Swedeen
 **/

/* Local Headers */
#include<occupancy_grid/laser_scan_processor.hpp>

/* ROS Headers */
#include<rclcpp/rclcpp.hpp>
#include<sensor_msgs/msg/laser_scan.hpp>
#include<geometry_msgs/msg/point_stamped.hpp>

/* Eigen Headers */
#include<Eigen/Dense>

/* TF Headers */
#include<tf2_ros/transform_listener.h>
#include<tf2_geometry_msgs/tf2_geometry_msgs.hpp>

/* C++ Headers */
#include<list>
#include<string>
#include<array>
#include<cmath>
#include<functional>

LaserScanProcessor::LaserScanProcessor(const std::string& node_name,
                                       const std::string& laser_scan_topic,
                                       const std::string& map_tf_id,
                                       const size_t       queue_length,
                                       const std::string& node_namespace)
 : rclcpp::Node(node_name, node_namespace),
   tf_buffer(this->get_clock()),
   transform_listener(this->tf_buffer),
   map_tf_id(map_tf_id),
   laser_scan_sub(this->create_subscription<sensor_msgs::msg::LaserScan>(
                    laser_scan_topic,
                    queue_length,
                    std::bind(&LaserScanProcessor::laserScanCallback, this, std::placeholders::_1)))
{}

std::list<Eigen::Matrix<double,2,1>> LaserScanProcessor::getObstacles()
{
  std::list<Eigen::Matrix<double,2,1>> output;

  rclcpp::spin_some(this->shared_from_this());

  if(not this->obstacles.empty())
  {
    output = std::move(this->obstacles);
    this->obstacles.clear();
  }
  return output;
}

void LaserScanProcessor::laserScanCallback(const sensor_msgs::msg::LaserScan& msg)
{
  // Get transform to map frame
  geometry_msgs::msg::TransformStamped temp_transform;
  try
  {
    temp_transform = this->tf_buffer.lookupTransform(this->map_tf_id,
                                                     msg.header.frame_id,
                                                     msg.header.stamp,
                                                     rclcpp::Duration(1, 0));
  }
  catch(const tf2::TransformException& ex)
  {
    RCLCPP_WARN_STREAM(this->get_logger(), ex.what());
    return;
  }

  // Add an edge that connects any visible obstacles
  const size_t msg_size = msg.ranges.size();
  for(size_t range_it = 0; range_it < msg_size; ++range_it)
  {
    // If there is a visible obstacle
    if((msg.ranges[range_it] > msg.range_min) and
       (msg.ranges[range_it] < msg.range_max))
    {
      Eigen::Matrix<double,2,1> output;
      geometry_msgs::msg::Point temp_point;
      geometry_msgs::msg::Point temp_point2;
      float                     point_yaw;

      /* Find point */
      // Find angle
      point_yaw = msg.angle_min + (msg.angle_increment * range_it);

      // Find x and y
      temp_point.x = msg.ranges[range_it] * std::cos(point_yaw);
      temp_point.y = msg.ranges[range_it] * std::sin(point_yaw);
      temp_point.z = 0;

      // Transform to map frame
      tf2::doTransform(temp_point, temp_point2, temp_transform);

      // Store result
      output[0] = temp_point2.x;
      output[1] = temp_point2.y;

      this->obstacles.emplace_front(std::move(output));
    }
  }
}
/* laser_scan_processor.cpp */
