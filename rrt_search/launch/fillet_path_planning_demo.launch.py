##
# @File: fillet_path_planning_demo.launch.py
#

from launch import LaunchDescription
from launch_ros.actions import Node
from launch.actions import SetEnvironmentVariable
from ament_index_python.packages import get_package_share_directory
import os
import yaml
import math

def generate_launch_description():
    with open(os.path.join(get_package_share_directory('occupancy_grid'), 'config', 'maze_world.yaml')) as occ_grid_yaml:
        occ_grid_dict = yaml.load(occ_grid_yaml, Loader=yaml.FullLoader)

        return LaunchDescription([
            Node(
                package='rrt_search',
                executable='fillet_path_planning_demo',
                name='fillet_path_planning_demo',
                output='screen',
                parameters=[{
                    'target_cost': 0.0,
                    'max_duration': 90.0,
                    'max_iteration': 0,
                    'near_radius': 3.0,
                    'edge_resolution': 0.01,
                    'num_target_samples': 100,
                    'batch_size': 100,
                    'max_curvature': 0.5, # 1.0/150.0,
                    'target_radius': 1.0,
                    'manhattan_demo': False,
                    #'start_point': [-13.5, -6.5, 0.0], # curve_obstacle_constraint world
                    #'target_point': [-3.0, 12.0, math.nan],
                    #'start_point': [-12.0, -2.5, 0.0], # sharp turn world
                    #'target_point': [-12.0, 2.5, math.nan],
                    #'start_point': [0.0, 0.0, 5.5], # spiral world
                    #'target_point': [-15.0, -15.0, math.nan],
                    #'start_point': [-20.0, -10.0, 1.5], # hallway world
                    #'target_point': [20.0, 0.0, math.nan],
                    'start_point': [-11.0, -22.5, 0.0], # maze world
                    'target_point': [2.5, 12.5, math.nan],
                    #'start_point': [15.0, 0.0, math.pi], # wall world
                    #'target_point': [-15.0, 0.0, math.nan],
                    'occupancy_grid' : occ_grid_dict,
                    'occupancy_grid.resolution': 0.01,
                    #'start_point': [0.0, 0.0, (7.0/6.0)*math.pi], # Manhattan world
                    #'target_point': [-9000.0, -3800.0, math.nan],
                    #'occupancy_grid':{
                    #    'line_width': -1.0,
                    #    'resolution': 0.1,
                    #    'origin': [-9700.0, -4650.0],
                    #    'width': 10700.0,
                    #    'height': 5650.0,
                    #    'nominal_altitude': 10.0,
                    #    'building_data_file': os.path.join(get_package_share_directory('occupancy_grid'), 'config', 'new_york_buildings_manhattan.csv'),
                    #    },
                    'nns_leaf_size': 1000,
                    'nns_num_threads': 64,
                    }],
                ),
            ])

